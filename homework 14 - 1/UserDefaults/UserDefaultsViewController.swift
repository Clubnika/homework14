//
//  UserDefaultsViewController.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 06/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit

class UserDefaultsViewController: UIViewController {

    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let firstTF  = MyUserDefaults.shared.firstTextField {
            firstTextField.text = firstTF }
        if let secondTF = MyUserDefaults.shared.secondTextField{
            secondTextField.text = secondTF
        }
        
    }
    
    @IBAction func first(_ sender: Any) {
        MyUserDefaults.shared.firstTextField = firstTextField.text
        print(MyUserDefaults.shared.firstTextField)
    }
    
    @IBAction func second(_ sender: Any) {
        MyUserDefaults.shared.secondTextField = secondTextField.text
        print(MyUserDefaults.shared.secondTextField)
    }

    
}
