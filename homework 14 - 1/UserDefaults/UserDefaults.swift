
import Foundation

class MyUserDefaults{
    static let shared = MyUserDefaults()
    
    private let kFirstTextField = "MyUserDefaults.kFirstTextField"
    private let kSecondTextField = "MyUserDefaults.kSecondTextField"
    
    var firstTextField: String? {
        set { UserDefaults.standard.set(newValue, forKey: kFirstTextField) }
        get { return UserDefaults.standard.string(forKey: kFirstTextField) }
    }
    
    var secondTextField: String? {
        set { UserDefaults.standard.set(newValue, forKey: kSecondTextField) }
        get { return UserDefaults.standard.string(forKey: kSecondTextField) }
    }
}
