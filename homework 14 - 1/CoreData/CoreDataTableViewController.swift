//
//  CoreDataTableViewController.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 10/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit
import CoreData


class CoreDataTableViewController: UITableViewController {
    
    var tasks = [NSManagedObject]()
    
    @IBAction func addName(_ sender: Any) {
        let alert = UIAlertController(title: "New task", message: "Add a new task", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action: UIAlertAction!) -> Void in
            let textField = alert.textFields![0]
            self.saveTask(textField.text!)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action: UIAlertAction!) -> Void in }
        alert.addTextField { (textField: UITextField!) -> Void in }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func saveTask(_ name: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =  NSEntityDescription.entity(forEntityName: "Person",in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKey: "name")
        
        do{
            try managedContext.save()
            tasks.append(person)
        } catch let err as NSError {
            print("Failed to save an task", err)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "\"The List To Do\""
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        do{
            tasks = try managedContext.fetch(fetchRequest)
        } catch let err as NSError {
            print("Fieled to fetch task", err)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // персон так как имено этот объект храниться у нас в БД
        let person = tasks[indexPath.row]
        // и теперь мы его извлекаем
        cell.textLabel?.text = person.value(forKey: "name") as? String //подписываем что бы получал строку!!!
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("выбрана ячейка -- \(indexPath.row)")
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let tasksDelete = tasks[indexPath.row]
        managedContext.delete(tasksDelete)
        
        do{
            try managedContext.save()
            print("выбрана ячейка -- \(indexPath.row)")
            tasks.remove(at: indexPath.row)
            tableView.reloadData()
        } catch let err as NSError {
            print("Failed to delete an item", err)
        }
    }
}
