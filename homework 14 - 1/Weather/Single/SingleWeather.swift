//
//  SingleWeather.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 13/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class SingleWeatherJSON {
    
    func loadData(completion: @escaping (SingleWeatherRealm) -> Void){
        Alamofire.request("https://api.openweathermap.org/data/2.5/weather?id=524901&APPID=7469ba9a71993814533b1886ce327460&units=metric&lang=ru").responseJSON { response in
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            let swr = UpdateSingleWeather().getDataJSON(json: json)

            DispatchQueue.main.async {
                completion(swr)
            }
        case .failure(let error):
            print(error)
            
        }
        }
    }
}

