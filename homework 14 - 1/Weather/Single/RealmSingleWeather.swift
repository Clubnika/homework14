//
//  RealmWeather.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 13/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

// задесь храним обьект для Realm'a
class SingleWeatherRealm: Object{
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var icon: String = ""
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var dt: Double = 0.0
}

// а здесь мы с ним работаем
class UpdateSingleWeather{
    
    let realm = try! Realm()
    
    func getDataJSON (json: JSON) ->  SingleWeatherRealm{
        let swr = SingleWeatherRealm()
        swr.name = json["name"].string!
        swr.desc = json["weather"][0]["description"].string!
        swr.icon = json["weather"][0]["icon"].string!
        swr.temp = json["main"]["temp"].double!
        swr.dt = json["dt"].double!
        
        return swr
    }
    
    func read () -> SingleWeatherRealm? {
        let realmSingleWeather = realm.objects(SingleWeatherRealm.self).first
        return realmSingleWeather
    }
    
    func readAll () -> Results<SingleWeatherRealm> {
        let realmSingleWeather = realm.objects(SingleWeatherRealm.self)
        return realmSingleWeather
    }
    
    func write(_ swr: SingleWeatherRealm){
        try! realm.write {
            realm.add(swr)
        }
    }
    
    func deleteAll (){
        try! realm.write {
            realm.delete(readAll())
        }
    }
}
