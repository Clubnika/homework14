//
//  ViewController.swift
//  homework 13 - 1
//
//  Created by Иван Морозов on 26/06/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit
//import RealmSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descripton: UILabel!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    var swr : SingleWeatherRealm?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        print("Вся прогрузка #1 ", UpdateSingleWeather().readAll())
        
        switch UpdateSingleWeather().read() == nil {
        case false:
            self.swr = UpdateSingleWeather().read()
//            record()
            self.name.text = swr?.name
            self.descripton.text = swr?.desc
            self.icon.text = swr?.icon
            self.temp.text = "\(Int(swr!.temp)) Cº"
            self.iconImage.image = UIImage(named: swr!.icon)
            let dateForm = DateFormatter()
            dateForm.dateFormat = "dd.MM HH:mm:ss"
            self.data.text = " \(dateForm.string(from: Date(timeIntervalSince1970:swr!.dt)))"
            print("время Перед JSON", self.data.text)
            
            fallthrough
        case true:
            SingleWeatherJSON().loadData { (swr) in
                self.swr = swr
//                self.record()
                self.name.text = swr.name
                self.descripton.text = swr.desc
                self.icon.text = swr.icon
                self.temp.text = "\(Int(swr.temp)) Cº"
                self.iconImage.image = UIImage(named: swr.icon)
                let dateForm = DateFormatter()
                dateForm.dateFormat = "dd.MM HH:mm:ss"
                self.data.text = " \(dateForm.string(from: Date(timeIntervalSince1970:swr.dt)))"
                print("время после JSON ", self.data.text )
                
                UpdateSingleWeather().deleteAll()
                UpdateSingleWeather().write(swr)
                
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // проверяем сколько элементов в реалме
        print("Вся прогрузка  ", UpdateSingleWeather().readAll())
    }
    
    // не работате как нужно, сначала прогружается viewDidLoad полность, а потом уже и эта функция! почему?
    func record (){
        if let swr = swr {
        self.name.text = swr.name
        self.descripton.text = swr.desc
        self.icon.text = swr.icon
        self.temp.text = "\(Int(swr.temp)) Cº"
        self.iconImage.image = UIImage(named: swr.icon)
        let dateForm = DateFormatter()
        dateForm.dateFormat = "dd.MM HH:mm:ss"
        self.data.text = " \(dateForm.string(from: Date(timeIntervalSince1970:swr.dt)))"
        print("время ", dateForm.string(from: Date(timeIntervalSince1970:swr.dt)))
        }
    }
}

