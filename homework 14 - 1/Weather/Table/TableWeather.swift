//
//  TableWeather.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 19/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class TableWeatherJSON {
    
     func loadData (completion: @escaping ([TableWeatherRealm]) -> Void) {
         request("https://api.openweathermap.org/data/2.5/forecast?id=551487&APPID=7469ba9a71993814533b1886ce327460&units=metric&lang=ru").responseJSON { responseJSON in

            //551487 - kazan
            //524901 - moscow
            
            switch responseJSON.result {
            case .success(let value):
                let json = JSON(value)
                let swr = UpdateTableWeather().getDataJSON(json: json)

                DispatchQueue.main.async { completion( swr ) }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
