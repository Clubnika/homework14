//
//  WeatherTableViewCell.swift
//  homework 13 - 1
//
//  Created by Иван Морозов on 30/06/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var date: UILabel!

}
