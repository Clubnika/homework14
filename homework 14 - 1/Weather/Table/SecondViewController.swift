//
//  SecondViewController.swift
//  homework 13 - 1
//
//  Created by Иван Морозов on 30/06/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit
import RealmSwift

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var weatherCells: [TableWeatherRealm] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch UpdateTableWeather().readAll() == nil {
        case false:
            weatherCells = [TableWeatherRealm](UpdateTableWeather().readAll())
            fallthrough
        case true:
            TableWeatherJSON().loadData { (twr) in
                self.weatherCells = twr
                self.tableView.reloadData()
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Загрузил во время нажатия ", weatherCells)
    }
    
    func AminateTable() {
        let cells = tableView.visibleCells
        
        let tableViewHeight = tableView.bounds.size.height
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        for cell in cells {
            UIView.animate(withDuration: 1.75, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}

extension SecondViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WeatherTableViewCell
        let i = weatherCells
        cell.desc.text = i[indexPath.row].desc
        cell.imageIcon.image = UIImage(named: i[indexPath.row].icon)
        cell.temp.text = "\(Int(i[indexPath.row].temp))  Cº"
        let dateForm = DateFormatter()
        dateForm.dateFormat = "dd.MM HH:mm"
        cell.date.text = " \(dateForm.string(from: Date(timeIntervalSince1970: i[indexPath.row].time)))"
 
        return cell
    }
    
}
