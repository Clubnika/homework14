//
//  RealmTableWeather.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 20/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class TableWeatherRealm: Object {
    
    @objc dynamic var desc: String = ""
    @objc dynamic var icon: String = ""
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var time: Double = 0.0
    
}

//class TableWeatherRealm

class UpdateTableWeather{
    
    let realm = try! Realm()
    
    func getDataJSON (json: JSON) -> [TableWeatherRealm]{
        
        var array: [TableWeatherRealm] = []
        
        if let list = json["list"].array{
            deleteAll()
            
            for json in list {
                let twr = TableWeatherRealm()
                twr.desc = json["weather"][0]["description"].string!
                twr.icon = json["weather"][0]["icon"].string!
                twr.temp = json["main"]["temp"].double!
                twr.time = json["dt"].double!
                array.append(twr)
                
                write(twr)
            }
        }
//        print("UPW: ", array)
        return array
    }
    
    func readAll () -> Results<TableWeatherRealm> {
        let twr = realm.objects(TableWeatherRealm.self)
        return twr
    }
    
    func write(_ twr: TableWeatherRealm){
        try! realm.write {
            realm.add(twr)
        }
    }
    
    func deleteAll() {
        try! realm.write {
            realm.delete(readAll())
        }
    }
}
