//
//  Realm.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 06/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import Foundation
import RealmSwift

class ToDo: Object {
    @objc dynamic var todo: String = ""
    @objc dynamic var check = false
}

class Persistanse{

    private let realm = try! Realm()
    
    func extract() -> Results<ToDo> {
        let allToDo = realm.objects(ToDo.self)
        return allToDo
    }
    
}
