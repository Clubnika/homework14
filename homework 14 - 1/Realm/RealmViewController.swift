//
//  RealmViewController.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 06/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//


import UIKit
import RealmSwift

class RealmViewController: UIViewController {

    let realm = try! Realm()
    
    @IBOutlet weak var myTableView: UITableView!

    var createAction: UIAlertAction!
    let indentifier = "ToDoCell"

    var listToDo: Results<ToDo>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listToDo = Persistanse().extract()
    }
    
    @IBAction func edit(_ sender: Any) {
        myTableView.isEditing = !myTableView.isEditing
        myTableView.reloadData()
    }
    
    @IBAction func add(_ sender: Any) {
        
        let addAlert = UIAlertController(title: "New tesk", message: nil, preferredStyle: .alert)
        let createAction = UIAlertAction(title: "Create", style: .default, handler: { (create) in
            let todo = ToDo()
            todo.todo = (addAlert.textFields?[0].text)!
            try! self.realm.write {
                self.realm.add(todo)
            }
            self.myTableView.reloadData()
        })
        
        addAlert.addAction(createAction)
        createAction.isEnabled = false
        self.createAction = createAction
        addAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        addAlert.addTextField { (textField) in
            textField.addTarget(self, action: #selector(self.tfForAlert), for: .editingChanged)
        }
        self.present(addAlert, animated: true, completion: nil)
    }
    
    @objc func tfForAlert (_ textField: UITextView){
        self.createAction.isEnabled = textField.text.count > 0
    }
}

extension RealmViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listToDo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: indentifier, for: indexPath) as! RealmTableViewCell
        let arr = listToDo[indexPath.row]
        cell.labToDo.text =   "\(arr.todo)"
        cell.labNum.text = "\(indexPath.row + 1)."
        
       // зачеркивание и чекмарки
        if arr.check == true {
            cell.accessoryType = .checkmark
            cell.labToDo.textColor = .gray
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.labToDo.text!)
            
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.labToDo.attributedText = attributeString
        } else {
            cell.accessoryType = .none
            cell.labToDo.textColor = .black
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.labToDo.text!)
            
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 0, range: NSMakeRange(0, attributeString.length))
            cell.labToDo.attributedText = attributeString
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write{
                realm.delete(listToDo[indexPath.row])
            }
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        try! realm.write {
            listToDo[indexPath.row].check = !listToDo[indexPath.row].check
        }
        tableView.reloadData()
    }
}

