//
//  RealmTableViewCell.swift
//  homework 14 - 1
//
//  Created by Иван Морозов on 08/07/2019.
//  Copyright © 2019 Иван Морозов. All rights reserved.
//

import UIKit

class RealmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labToDo: UILabel!
    @IBOutlet weak var labNum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    

}
